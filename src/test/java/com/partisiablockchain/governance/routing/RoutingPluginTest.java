package com.partisiablockchain.governance.routing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class RoutingPluginTest {

  private static final BlockchainAddress ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private static final BlockchainAddress TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  private final RoutingPlugin plugin = new RoutingPlugin();

  @Test
  public void route() {
    String shard0 = "Shard0";
    String shard1 = "Shard1";
    FixedList<String> shards = FixedList.create(List.of(shard0, shard1));
    Assertions.assertThat(plugin.route(shards, ONE)).isEqualTo(shard1);
    Assertions.assertThat(plugin.route(shards, TWO)).isEqualTo(shard0);
  }

  @Test
  public void route_emptyShards() {
    FixedList<String> shards = FixedList.create();
    Assertions.assertThat(plugin.route(shards, ONE)).isNull();
  }

  @Test
  public void route_nullShards() {
    Assertions.assertThat(plugin.route(null, ONE)).isNull();
  }

  @Test
  public void getGlobalStateClass() {
    Assertions.assertThat(plugin.getGlobalStateClass()).isEqualTo(StateVoid.class);
  }

  @Test
  public void migrateGlobal() {
    Assertions.assertThat(plugin.migrateGlobal(null, null)).isNull();
  }
}
