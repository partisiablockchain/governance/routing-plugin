package com.partisiablockchain.governance.routing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainRoutingPlugin;
import com.secata.tools.immutable.FixedList;
import com.secata.util.NumericConversion;

/** Routing plugin. */
public final class RoutingPlugin extends BlockchainRoutingPlugin {

  /**
   * Determine which shard the target address belongs to.
   *
   * @param shards list of known shards
   * @param target the address to determine location of
   * @return the id of the shard where target is located
   */
  @Override
  public String route(FixedList<String> shards, BlockchainAddress target) {
    if (shards == null || shards.isEmpty()) {
      return null;
    } else {
      int value = Math.abs(NumericConversion.intFromBytes(target.getIdentifier(), 16));
      return shards.get(value % shards.size());
    }
  }
}
